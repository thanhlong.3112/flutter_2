class ProductItem {
  final String id;
  final String title;
  final String description;
  final String price;
  final String imageUrl;
  ProductItem(
      {required this.id,
      required this.title,
      required this.price,
      required this.description,
      required this.imageUrl});

  void insert(int index, ProductItem result) {}

  void removeAt(int index) {}
}
