import 'package:flutter/material.dart';
import 'package:flutter_2/detail_screen.dart';
import 'package:flutter_2/edit_products_screen.dart';
import 'package:flutter_2/my_shop_screen.dart';
import 'package:flutter_2/your_products_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: TextTheme(
            headline1: TextStyle(fontSize: 18, color: Colors.black),
            headline2: TextStyle(fontSize: 17, color: Colors.grey),
            headline3: TextStyle(fontSize: 18, color: Colors.purple),
            headline4: TextStyle(
                fontSize: 50,
                fontWeight: FontWeight.bold,
                fontFamily: 'Anton')),
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
                primary: Colors.purple,
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)))),
        primarySwatch: Colors.purple,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
      routes: {
        YourProducts.routeName: (context) => const YourProducts(),
      },

      onGenerateRoute: generateRoute, // generate router
      onUnknownRoute: (settings) => MaterialPageRoute(
        builder: (context) => const Text('Unknown route'),
      ),
    );
  }

  Route<dynamic>? generateRoute(RouteSettings settings) {
    if (settings.name == EditProductsScreen.routeName) {
      return MaterialPageRoute(
          builder: (_) {
            return const EditProductsScreen();
          },
          settings: settings);
    } else if (settings.name == MyShopScreen.routeName) {
      return MaterialPageRoute(
          builder: (_) {
            return const MyShopScreen();
          },
          settings: settings);
    } else if (settings.name == DetailScreen.routeName) {
      return MaterialPageRoute(
          builder: (_) {
            return const DetailScreen();
          },
          settings: settings);
    } else
      return null;
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _showForm = true;
  void _changeForm() {
    setState(() {
      _showForm = !_showForm;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment(
                  0.8, 0.0), // 10% of the width, so there are ten blinds.
              colors: <Color>[
                Color(0xffb7a1f0),
                Color(0xfffadbbb)
              ], // red to yellow
            ),
          ),
        ),
        Center(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
                  transform: Matrix4.rotationZ(-0.1),
                  decoration: BoxDecoration(
                    color: Colors.deepOrange.shade900,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Text(
                    'My Shop',
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        ?.copyWith(color: Colors.white),
                  ),
                ),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 8.0,
                  margin: const EdgeInsets.all(20),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Form(
                      child: Column(
                        children: [
                          TextFormField(
                            decoration: const InputDecoration(
                              labelText: 'Email',
                            ),
                            style: Theme.of(context).textTheme.headline1,
                          ),
                          TextFormField(
                            decoration: const InputDecoration(
                              labelText: 'Password',
                            ),
                            obscureText: true,
                            style: Theme.of(context).textTheme.headline1,
                          ),
                          if (!_showForm)
                            TextFormField(
                              decoration: const InputDecoration(
                                labelText: 'Confirm Password',
                              ),
                              obscureText: true,
                              style: Theme.of(context).textTheme.headline1,
                            ),
                          const SizedBox(
                            height: 20,
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const YourProducts()));
                            },
                            child: Text(_showForm ? 'LOGIN' : 'SIGN UP'),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextButton(
                            onPressed: _changeForm,
                            child: Text(
                                '${_showForm ? 'SIGNUP' : 'LOGIN'} INSTEAD'),
                            style: TextButton.styleFrom(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 8),
                              textStyle: Theme.of(context).textTheme.headline3,
                              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
