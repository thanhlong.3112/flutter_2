import 'package:flutter/material.dart';
import 'package:flutter_2/drawer.dart';
import 'package:flutter_2/edit_products_screen.dart';
import 'package:flutter_2/my_shop_screen.dart';

import 'products_items.dart';

class YourProducts extends StatefulWidget {
  static const routeName = '/order-list';
  const YourProducts({Key? key}) : super(key: key);

  @override
  _YourProductsState createState() => _YourProductsState();
}

class _YourProductsState extends State<YourProducts> {
  final List<ProductItem> items = <ProductItem>[
    ProductItem(
        id: '1',
        title: 'Product 111',
        price: '100',
        imageUrl:
            'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
        description: 'Ok'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Your Products'),
          actions: [
            IconButton(
                onPressed: () async {
                  final result = await Navigator.pushNamed(
                      context, EditProductsScreen.routeName);

                  setState(() {
                    if (result != null) {
                      items.add(result as ProductItem);
                    }
                  });
                },
                icon: Icon(Icons.add))
          ],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Text('Drawer Header'),
              ),
              ListTile(
                title: const Text('Your Product'),
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    YourProducts.routeName,
                  );
                },
              ),
              Divider(),
              ListTile(
                title: const Text('My Shop'),
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    MyShopScreen.routeName,
                    arguments: items,
                  );
                },
              ),
            ],
          ),
        ),
        body: ListView.separated(
            itemBuilder: (BuildContext context, int index) {
              final ProductItem item = items[index];
              return ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(item.imageUrl),
                  ),
                  title: Text(item.title),
                  trailing: Row(mainAxisSize: MainAxisSize.min, children: [
                    IconButton(
                        icon: const Icon(
                          Icons.edit,
                          color: Colors.purple,
                        ),
                        onPressed: () async {
                          final result = await Navigator.pushNamed(
                            context,
                            EditProductsScreen.routeName,
                            arguments: item,
                          );
                          if (result != null) {
                            item.removeAt(index);
                            item.insert(index, result as ProductItem);
                            setState(() {});
                          }
                        }),
                    IconButton(
                        icon: const Icon(
                          Icons.safety_divider,
                          color: Colors.purple,
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MyShopScreen()));
                        }),
                    IconButton(
                        icon: const Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          setState(() {
                            items.remove(item);
                          });
                        }),
                  ]));
            },
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
            itemCount: items.length));
  }
}
