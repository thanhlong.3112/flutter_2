import 'package:flutter/material.dart';
import 'package:flutter_2/products_items.dart';

class EditProductsScreen extends StatefulWidget {
  static const routeName = '/order-update';
  final ProductItem? item;
  const EditProductsScreen({Key? key, this.item}) : super(key: key);

  @override
  _EditProductsScreenState createState() => _EditProductsScreenState();
}

class _EditProductsScreenState extends State<EditProductsScreen> {
  final _formKey = GlobalKey<FormState>();
  final _titleController = TextEditingController();
  final _imageUrlController = TextEditingController();
  final _priceController = TextEditingController();
  final _descriptionController = TextEditingController();

  final Map<String, dynamic> _initValues = {
    'id': 1,
    'title': '',
    'price': '',
    'description': '',
    'imageUrl': '',
  };

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    final args = ModalRoute.of(context)!.settings.arguments as ProductItem?;

    if (args != null) {
      _initValues['id'] = args.id;
      _initValues['name'] = args.title;
      _initValues['price'] = args.price.toString();
      _initValues['description'] = args.description;
      _initValues['image'] = args.imageUrl;

      _imageUrlController.text = args.imageUrl;
      _titleController.text = args.title;
      _priceController.text = args.price;
      _descriptionController.text = args.description;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Products'),
        actions: [IconButton(onPressed: _saveForm, icon: Icon(Icons.save))],
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  decoration: const InputDecoration(labelText: ('Title')),
                  style: Theme.of(context).textTheme.headline1,
                  controller: _titleController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please provide a value';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    _initValues['title'] = value;
                  },
                ),
                TextFormField(
                  decoration: const InputDecoration(labelText: ('Price')),
                  style: Theme.of(context).textTheme.headline1,
                  controller: _priceController,
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter a price';
                    }
                    return null;
                  },
                  onSaved: (String? value) {
                    _initValues['price'] = value ?? '';
                  },
                ),
                TextFormField(
                  decoration: const InputDecoration(labelText: ('Description')),
                  style: Theme.of(context).textTheme.headline1,
                  controller: _descriptionController,
                  maxLines: 3,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter a description';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    _initValues['description'] = value;
                  },
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  children: [
                    Container(
                      width: 120,
                      height: 120,
                      margin: EdgeInsets.only(top: 10, right: 15),
                      decoration: BoxDecoration(border: Border.all(width: 1)),
                      child: Container(
                        child: _imageUrlController.text.isEmpty
                            ? Text('Enter a URL')
                            : FittedBox(
                                child: Image.network(_imageUrlController.text),
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        controller: _imageUrlController,
                        decoration:
                            const InputDecoration(labelText: ('Image URL')),
                        style: Theme.of(context).textTheme.headline1,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter an image URL';
                          }

                          return null;
                        },
                        onChanged: (value) {
                          _initValues['imageUrl'] = value;
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _saveForm() {
    if (_formKey.currentState?.validate() == true) {
      _formKey.currentState?.save();
      Navigator.pop(
          context,
          ProductItem(
              id: _initValues['id'].toString(),
              title: _initValues['title'],
              price: _initValues['price'].toString(),
              description: _initValues['description'],
              imageUrl: _initValues['imageUrl']));
    }
  }
}
