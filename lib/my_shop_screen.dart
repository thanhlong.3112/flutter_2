import 'package:flutter/material.dart';
import 'package:flutter_2/detail_screen.dart';
import 'package:flutter_2/drawer.dart';
import 'your_products_screen.dart';
import 'products_items.dart';

class MyShopScreen extends StatefulWidget {
  static const routeName = '/my-shop';
  const MyShopScreen({Key? key}) : super(key: key);

  @override
  _MyShopScreenState createState() => _MyShopScreenState();
}

class _MyShopScreenState extends State<MyShopScreen> {
  /* final List<ProductItem> items = <ProductItem>[
    ProductItem(
        id: '1',
        title: 'Product 111',
        price: '100',
        imageUrl:
            'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
        description: 'Ok'),
  ]; */

  @override
  void didChangeDependencies() {
    final args = ModalRoute.of(context)!.settings.arguments as ProductItem?;
    if (args != null) {}

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ProductItem?;
    return Scaffold(
      appBar: AppBar(
        title: const Text('MyShop'),
        actions: [
          Icon(
            Icons.more,
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: GridView.builder(
          itemBuilder: (BuildContext context, int index) {
            return GridTile(
              child: GestureDetector(
                child: FittedBox(
                  child: Image.network(args!.imageUrl),
                  fit: BoxFit.fill,
                ),
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    DetailScreen.routeName,
                  );
                },
              ),
              footer: GridTileBar(
                backgroundColor: Colors.black54,
                leading: IconButton(
                  icon: Icon(Icons.favorite_outline),
                  onPressed: () {},
                ),
                title: Text(
                  args.title,
                  textAlign: TextAlign.center,
                ),
                trailing: IconButton(
                    onPressed: () {}, icon: Icon(Icons.shopping_cart)),
              ),
            );
          },
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 20,
              mainAxisSpacing: 20,
              childAspectRatio: 3 / 2),
        ),
      ),
    );
  }
}
